const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require("body-parser");
const csrf = require('csurf');
const cookieParser = require('cookie-parser');
const sessions = require('express-session');
const cors = require("cors");
const url = require("url");
const http = require("http");
var fs = require('fs');
const path = require('path');

//include connection file for the db
const connectDB = require('./server/database/connection');

const app = express();

//setup the origin for the cors or cross site request forgery/security
var corsOptions = {
    origin: "http://localhost:3000"
  };
  
app.use(cors(corsOptions));

var csrfProtection = csrf({ cookie: true });

//setup the .env file for the database credentials
dotenv.config( { path : 'config.env'} );

//setup the port for the application
const PORT = process.env.PORT || 8080


// log requests
app.use(morgan('tiny'));
var imgModel = require('./server/model/imgmodel');
const fileUpload = require('express-fileupload');
//const multer = require('multer');
// app.use( multer({ storage: fileStorage,fileFilter(req, file, cb) {
//     if (!file.originalname.match(/\.(png|jpg)$/)) { 
//        // upload only png and jpg format
//        return cb(new Error('Please upload a Image'))
//      }
//    cb(undefined, true)
// } }).single('image'));

//2
// var fileStorage=multer.diskStorage({
//     destination:function(req,file,cb){
//         cb(null,'uploads');
//     },  filename:function(req,file,cb){
//         cb(null,file.originalname);
//     }
// });

//2
// const imageStore=multer({ storage: fileStorage,fileFilter(req, file, cb) {
//     if (!file.originalname.match(/\.(png|jpg|PNG)$/)) { 
//        // upload only png and jpg format
//        return cb(new Error('Please upload a Image'))
//      }
//    cb(undefined, true);
// }});

//2
// logger("info","eror");
app.use('/', express.static(path.join(__dirname, 'uploads')));

app.get('/imgupl',(req,res)=>{
    // imgModel.find({}, (err, items) => {
    //     if (err) {
    //         console.log(err);
    //         res.status(500).send('An error occurred', err);
    //     }
    //     else {
           // res.render('image_form', { items: items });
           res.render('image_form');
    //     }
    // });
   
});

// app.post("/upload", imageStore.single('image'),(req,res)=>{
   
//     var obj = {
//         name: req.body.name,
//         desc: req.body.desc,
//         image: {
//             //data:fs.readFileSync(path.resolve(req.file.originalname)),
//             data: fs.readFileSync(path.join(__dirname+'/uploads/'+req.file.originalname)),
//             contentType: 'image/png'
//         }
//     }

//     var filename=req.files;
//     filename.mv("/uploads"+req.file.originalname,function(err){
//         if(err){
//             res.send("error");
//         }else{
//             res.send("file uploaded");
//         }
//     });
//     // imgModel.create(obj, (err, item) => {
//     //     if (err) {
//     //         console.log(err);
//     //     }
//     //     else {
//     //         // item.save();
//     //         res.redirect('/imgupl');
//     //     }
//     // });
    
//     // if(!file){
//     //     const error=new Error("please upload af file");
//     //     error.httpStatusCode=400;
//     //     return next(error);
//     // }

//     //res.send(req.body.file);
// });

app.use(fileUpload({
    //limits: { fileSize: 50 * 1024 * 1024 },
    useTempFiles : true,
    tempFileDir : '/tmp/'
  }));

app.post("/upload",(req,res)=>{
   
    // var obj = {
    //     name: req.body.name,
    //     desc: req.body.desc,
    //     image: {
    //         //data:fs.readFileSync(path.resolve(req.file.originalname)),
    //         data: fs.readFileSync(path.join(__dirname+'/uploads/'+req.file.originalname)),
    //         contentType: 'image/png'
    //     }
    // }
    console.log(req.files);
    var filename=req.files.image;
    filename.mv("uploads/"+req.files.image.name,function(err){
        if(err){
            res.send(err);
        }else{
            res.send("file uploaded");
        }
    });
    // imgModel.create(obj, (err, item) => {
    //     if (err) {
    //         console.log(err);
    //     }
    //     else {
    //         // item.save();
    //         res.redirect('/imgupl');
    //     }
    // });
    
    // if(!file){
    //     const error=new Error("please upload af file");
    //     error.httpStatusCode=400;
    //     return next(error);
    // }

    //res.send(req.body.file);
});

//setup session middleware
app.use(sessions({
    secret: "secretkey",
    saveUninitialized:true,
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 30 },
    resave: true 
}));

// mongodb connection
connectDB();

// parse request to body-parser
app.use(bodyparser.urlencoded({ extended : true}))
app.use(bodyparser.json());
// set view engine
app.set("view engine", "ejs")
//app.set("views", path.resolve(__dirname, "views/ejs"))
app.use(cookieParser());

// load assets
app.use('/css', express.static(path.resolve(__dirname, "assets/css")))
app.use('/img', express.static(path.resolve(__dirname, "assets/img")))
app.use('/js', express.static(path.resolve(__dirname, "assets/js")))


// load routers
app.use('/', require('./server/routes/router'));


app.listen(PORT, ()=> { console.log(`Server is running on http://localhost:${PORT}`)});
const express = require('express');
const route = express.Router()
const csrf = require('csurf');
const sessions = require('express-session');
const services = require('../services/render');
const controller = require('../controller/user_controller');
const product_controller = require('../controller/product_controller');
const bcrypt = require("bcryptjs");
const multer = require('multer');

//set csrf protection 
var csrfProtection = csrf({ cookie: true });


var fileStorage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'uploads');
    },  filename:function(req,file,cb){
        cb(null,file.fieldname+"-"+file.originalname);
    }
});


//gets home page design
route.get('/', services.homeRoutes);

//used for the registration
route.get("/registration", csrfProtection,(req,res)=>{
    res.render("registration_user", { csrfToken: req.csrfToken() });
});

//used for login
route.get("/login",(req,res)=>{
    res.render("login_user");
});

//used  for displaying all the products
route.get('/products', product_controller.getData);

//used for getting single product
route.get('/get_single_product', product_controller.findSingleProduct);

//used for adding product page
route.get('/add-product', services.addProduct)

//used  for updating product page
route.get('/update-product', services.updateProduct)


// used for submission for add,update and delete products 
route.post('/add_product', product_controller.create);
route.get('/api/get_products', product_controller.getData);
route.post('/update_product_data', product_controller.update);
route.get('/delete-product/:id', product_controller.delete);


//route for logout
route.get('/logout', function (req, res) {
    req.session.destroy();
    // req.flash('success', 'Login Again Here');
    res.redirect('/login');
});

 // used for the submission of registration data
 route.post('/register', csrfProtection,controller.create);

  // used for the submission of login data
route.post('/loginuser', controller.find);

// route.post('/upload', product_controller.find);

 // used for unauthorize access of the request.
route.get('*',(req,res)=>{
    res.render('404_page');
});

module.exports = route
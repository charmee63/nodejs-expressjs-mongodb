const mongoose = require("mongoose");
const ProductSchema = new mongoose.Schema({
    product_name : {
        type : String,
        required:[true,"product name is required"]
    },
    product_description : {
        type: String,
        required: [true,"product description is required"],
    },
    price : {
        type: Number,
        required: [true,"price is required"],
    },
    quantity : {
        type: Number,
        required: [true,"quantity is required"],
    },
  
}) ;

module.exports = mongoose.model("products",ProductSchema);
var Userdb = require('../model/model');
const express = require('express');
const app = express();
const sessions = require('express-session');

const bcrypt = require("bcryptjs");
var fs = require('fs');
var ProductSchema = require('../model/product');
const path = require('path');
var session;
let hashedPass = '';
// create and save new user
exports.create =async (req,res)=>{
    const {name, email, gender,type, password} = req.body;
    // validate request
    if(!req.body){
        res.render("registration_user",{ message : "Content can not be emtpy!"});
    }

    //hased password
    hashedPass=await bcrypt.hash(password,1);

    await  Userdb.findOne({email: email})
    .then(data =>{
        if(!data){
                  // new user
                  const user = new Userdb({
                    name : name,
                    email : email,
                    gender: gender,
                    type: type,
                    password:hashedPass,
                });
    
                // save user in the database
                user
                    .save(user)
                    .then(data => {
                        res.redirect('/login');
                    })
                    .catch(err =>{
                        res.render("registration_user",{
                            message : err.message || "Some error occurred while creating a create operation"
                        });
                    });
          
        }else{
            res.render("registration_user",{ message : " found user with email "+ email})
               
        }
    })
    .catch(err =>{
        res.render("login_user",{ message: "Erro retrieving user with email " + email})
    })
 

}

//  retrive and return a single user based on email
exports.find =async (req, res)=>{
    const {email, password} = req.body;
    if(email && password){

        await  Userdb.findOne({email: email})
            .then(data =>{
                if(!data){
                    res.render("login_user",{ message : "Not found user with email "+ email})
                }else{
                    let isEqual = bcrypt.compare(password,data.password);
                    if(isEqual){
                         session=req.session;
                        session.name=data.name;
                        session.loggedin=true;
                        if(data.name=="admin" && email=="admin1@gmail.com") {
                            res.render('admin_profile',{message:data.name});
                        }else{
                            ProductSchema.find({})
                            .then(user => {
                                res.render("user_profile",{products:user,message:data.name});
                               
                            })
                            .catch(err => {
                                res.render("login_user",{ message : err.message || "Error Occurred while retriving product_name information" })
                            })

                       
                        }
                  
                    }else{
                        res.render("login_user",{message:"email or password does not match"})
                    }
                       
                }
            })
            .catch(err =>{
                res.render("login_user",{ message: "Erro retrieving user with email " + email})
            })

    }else{
       await Userdb.find()
            .then(user => {
                res.render("login_user",{message:"user not found"});
            })
            .catch(err => {
                res.render("login_user",{ message : err.message || "Error Occurred while retriving user information" })
            })
    }

    
}


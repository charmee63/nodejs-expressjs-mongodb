var ProductSchema = require('../model/product');
const sessions = require('express-session');
var session;

// create and save new product
exports.create = (req,res)=>{

    var name = req.body.product_name;
    var email = req.body.product_description;
  
    // validate request
    if(!req.body){
        res.render('add_product',{message : "Content can not be emtpy!"});
    }

    // new user
    const user = new ProductSchema({
        product_name : req.body.product_name,
        product_description : req.body.product_description,
        price: req.body.price,
        quantity : req.body.quantity
    })


    // save user in the database
    user
        .save(user)
        .then(data => {
            //res.send(data)
            res.render('add_product',{add_status:"product added successfully"});
        })
        .catch(err =>{
            res.render('add_product',{ add_status : err.message || "Some error occurred while creating a create operation"});
    
        });

}

// retrive and return a single products
exports.find =async (req, res)=>{

    if(req.body.product_name){
        const product_name = req.body.product_name;
        const price = req.body.price;
        
       await ProductSchema.findOne({product_name: product_name})
            .then(data =>{
                if(!data){
                    res.render("login_user",{ message : "Not found product  with prouct name "+ product_name})
                }else{
                    session=req.session;
                    session.product_name=req.body.product_name;
                    res.render('user_profile',{message:product_name});
                }
            })
            .catch(err =>{
                res.render("login_user",{ message: "Error retrieving  product  with prouct name " + product_name})
            })

    }else{
       await ProductSchema.find()
            .then(user => {
                res.render("login_user",{message:"product not found"});
               
            })
            .catch(err => {
                res.render("login_user",{ message : err.message || "Error Occurred while retriving product_name information" })
            })
    }

    
}

// Update a new idetified product by prooduct id
exports.update =async (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }

    const id = req.body.id;
   await ProductSchema.findByIdAndUpdate(id, req.body, { useFindAndModify: true})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update product with ${id}. Maybe product not found!`})
            }else{
                res.redirect('/update-product?id='+id);
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update product information"})
        })
}

// Delete a user with specified product id in the request
exports.delete =async (req, res)=>{
    const id = req.params.id;
    await ProductSchema.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.redirect("/products");
            }else{
                 res.redirect("/products");
               
            }
        })
        .catch(err =>{
            res.redirect("/products");
           
        });
}

//retrieve and return all products for admin login
exports.getData =async (req, res)=>{
           await ProductSchema.find({})
            .then(data =>{
                if(!data){
                    res.render("index",{ message : "Not found product "})
                }else{
                    if(req.session.loggedin){
                        res.render("index",{message:req.session.name,products:data});
                    }else{
                        res.redirect("/login");
                    }
                    
                }
            })
            .catch(err =>{
               res.render("index",{ message: "Erro retrieving products "})
            })
    
}

//retrieve and return specified product
exports.findSingleProduct =async (req, res)=>{
    await ProductSchema.findOne({id: req.params.id})
     .then(data =>{
         if(!data){
             res.render("index",{ message : "Not found product "})
         }else{
             res.send(data);
            
         }
     })
     .catch(err =>{
        res.render("index",{ message: "Erro retrieving products "})
     })

}

//retrieve and return all products for user login
exports.getProducts =async (req, res)=>{
   await ProductSchema.find({})
     .then(data =>{
         if(!data){
             res.render("all_products",{ message : "Not found product "})
         }else{
             if(req.session.loggedin){
                 res.render("all_products",{message:req.session.name,products:data});
             }else{
                 res.redirect("/login");
             }
             
         }
     })
     .catch(err =>{
        res.render("all_products",{ message: "Erro retrieving products "})
     })

}
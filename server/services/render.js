const axios = require('axios');


exports.homeRoutes = (req, res) => {
    res.render('main_index');
    
}

exports.getProducts =async (req, res) => {
    // Make a get request to /api/users
   await axios.get('http://localhost:3000/api/get_products')
        .then(function(response){
            res.render('index', { products : response.data });
        })
        .catch(err =>{
            res.send(err);
        })

    
}

exports.addProduct = (req, res) =>{
    res.render('add_product');
}

exports.updateProduct =async (req, res) =>{
    await axios.get('http://localhost:3000/get_single_product', { params : { id : req.query.id }})
        .then(function(productdata){
            res.render("update_product", { products : productdata.data})
        })
        .catch(err =>{
            res.send(err);
        })
}